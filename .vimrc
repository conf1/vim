set gdefault "s///g will be used by default
syntax enable " enable synta processing
set tabstop=2 " number of visual espaces per TAB
set expandtab " tabs are spaces
set showcmd " show comman in bottom bar
set cursorline " highlight current line
set wildmenu " visualt autocomplete for command menu
set showmatch " highlight matching [{()}]
set hlsearch " higlight matches
" move to beggining/end line
nnoremap B ^
nnoremap E $
set fileformat=unix
set pastetoggle=<F6> " toggles between paste and no paste mode
set ignorecase          " Do case insensitive matching
set smartcase           " Do smart case matching
set incsearch           " Incremental search
filetype plugin indent on
